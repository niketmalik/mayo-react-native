# Mayo React Native Client

### Getting Started

##### Node & Watchman

```bash
brew install node
brew install watchman
```

##### Xcode & CocoaPods

```bash
sudo gem install cocoapods
```

##### Install dependencies

```bash
yarn
```

#### Running application

##### iOS

```bash
yarn ios
```

##### Android

```bash
yarn android
```
