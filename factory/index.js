import { Platform, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import APIHelper from './APIHelper';

/**
 * Mayo Factory
 * @constructor
 */
function Mayo() {
  /** @const {boolean} */
  this.isDev = process.env.NODE_ENV === 'development';
}

/**
 * API helper
 * @return {!APIHelper}
 */
Mayo.prototype.api = new APIHelper();

/**
 * Get user info
 * @return {!Promise<!Object<string, ?>, ?>}
 */
Mayo.prototype.registerUser = function () {
  return AsyncStorage.clear().then(() =>
    this.api
      .post('/user/register', {
        platform: Platform.OS.toUpperCase(),
        provider: 'ANONYMOUS',
      })
      .then(({ uid, authToken, refreshToken }) => {
        return AsyncStorage.setItem('AUTH_TOKEN', authToken)
          .then(() =>
            AsyncStorage.setItem(
              'AUTH_TOKEN_EXPIRES_AT',
              new Date(Date.now() + 59 * 60 * 1000).getTime().toString()
            )
          )
          .then(() => AsyncStorage.setItem('REFRESH_TOKEN', refreshToken))
          .then(() => AsyncStorage.setItem('USER_ID', uid))
          .then(() => uid);
      })
  );
};

/**
 * Get user info
 * @return {!Promise<!Object<string, ?>, ?>}
 */
Mayo.prototype.getUser = function () {
  return this.api
    .authenticate()
    .get('/user/me')
    .catch((err) => {
      if (err.code === 403) {
        Alert.alert(
          'Please authenticate before proceeding.',
          '',
          [
            {
              text: 'OK',
              onPress: () => this.registerUser().then(() => this.getUser()),
            },
          ],
          {
            cancelable: false,
          }
        );
      } else {
        throw err;
      }
    });
};

/**
 * Set user's device token
 * @param {string} deviceToken
 * @return {!Promise<!Object<string, ?>, ?>}
 */
Mayo.prototype.setDeviceToken = function (deviceToken) {
  return this.api.authenticate().post('/user/deviceToken', {
    deviceToken: deviceToken,
  });
};

module.exports = new Mayo();
